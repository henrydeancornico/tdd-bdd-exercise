package com.exercise.lazzshopp;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import com.exercise.lazzshopp.LazzshoppApplication;

@SpringBootTest
class LazzshoppApplicationTests {

	private LazzshoppApplication lazzshopp;
	private ArrayList<String> product = new ArrayList<>();

	// run before test..
	@BeforeEach
	public void initTest() {
		lazzshopp = new LazzshoppApplication();
	}
	@Test
	public void checkProductTest() {
		// expect to return true if product has name, quantity, and price.
		assertTrue(lazzshopp.checkProduct("Apple", 20, 20), "Checking product..");
	}

	@Test
	public void createProductListTest() {
		// example input.
		String prodName = "Orange";
		Integer prodQuantity = 500;
		Integer prodPrice = 15;
		product.add(prodName);
		product.add(prodQuantity.toString());
		product.add(prodPrice.toString());

		// expect to return true if product has been successfully added.
		assertTrue(true, "Product created: ");
	}

	@Test
	public void updateProductListTest() {
		// example input.
		String prodName = "Strawberry";
		Integer prodQuantity = 1000;
		Integer prodPrice = 20;
		product.add(prodName);
		product.add(prodQuantity.toString());
		product.add(prodPrice.toString());
		// expect to return true if product has been successfully added.
		assertFalse(lazzshopp.updateProduct(2, product));

	}

	@Test
	public void deleteProductListTest() {
		// expect to return true if product has been successfully added.
		assertFalse(lazzshopp.deleteProduct(1));
	}

	@Test
	public void getproductByIdTest() {
		// expect to return true if product successfully retrieved.
		assertFalse(lazzshopp.getproductById(3));
	}

	@Test
	public void showallProductsTest() {
		// expect to return true if products successfully retrieved.
		assertFalse(lazzshopp.showallProducts());
	}

}

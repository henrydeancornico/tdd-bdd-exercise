package com.exercise.lazzshopp.stepdefinitions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class OrderCheckoutStepDef {
	    @Given("the client already added a list of <string> on the cart")
	    public void theClientAlreadyAddedAListOfStringOnTheCart() {
	        System.out.println("Client added list");
	    }

	    @And("the client proceeded to checkout")
	    public void theClientProceededToCheckout() {
	        System.out.println("checkout");
	    }

	    @When("the system processes the <string{int}> in the cart")
	    public void theSystemProcessesTheStringInTheCart(int arg0) {
	        System.out.println(arg0);
	    }

	    @Then("the client should see the <string{int}> of the list <string{int}>")
	    public void theClientShouldSeeTheStringOfTheListString(int arg0, int arg1) {
	        System.out.println(arg0 + arg1);
	    }

	    @And("the client should see the list of <string{int}>")
	    public void theClientShouldSeeTheListOfString(int arg0) {
	        System.out.println(arg0);
	    }
	}


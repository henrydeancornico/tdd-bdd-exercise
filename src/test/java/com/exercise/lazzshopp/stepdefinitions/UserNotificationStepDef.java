package com.exercise.lazzshopp.stepdefinitions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class UserNotificationStepDef {

    @Given("the client has entered their <string> and <string{int}>")
    public void theClientHasInputtedTheirStringAndString(int arg0) {
        System.out.println(arg0);
    }

    @When("the system saves the <string> and <string{int}> inside the database.")
    public void theSystemRegistersTheStringAndStringInsideTheDatabase(int arg0) {
        System.out.println(arg0);
    }

    @Then("the client will get a <string{int}> that his account has been created.")
    public void theClientGetsAStringThatHisAccountHasBeenCreated(int arg0) {
        System.out.println(arg0);
    }
    
}

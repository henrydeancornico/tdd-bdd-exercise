Feature: Order Checkout

  Scenario Outline: View the total amount of items in the cart to know how much will be the payment
    Given the client already added a list of <string> on the cart
    And the client proceeded to checkout
    When the system processes the <string1> in the cart
    Then the client should see the <string2> of the list <string21>
    And the client should see the list of <string3>
    Examples:
      | string | string1 | string2 | string21 | string3 |
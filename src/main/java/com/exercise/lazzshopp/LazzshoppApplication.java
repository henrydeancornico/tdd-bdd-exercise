package com.exercise.lazzshopp;

import java.util.ArrayList;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LazzshoppApplication {

	public static void main(String[] args) {
		SpringApplication.run(LazzshoppApplication.class, args);
	}
	
	// initialize global 2d ArrayList productList to store product array.
		ArrayList<ArrayList<String>> productList = new ArrayList<>();

		//	check if product has name, quantity, and price.
		public boolean checkProduct(String prodName, Integer prodQuantity, Integer prodPrice) {
			if (prodName != null && prodPrice != 0 && prodQuantity != 0) {
				System.out.println("Product is good!");
				return true;
			} else {
				return false;
			}
		}

		public boolean createProduct(ArrayList<String> product) {
			// store product in productList.
			productList.add(product);
			// check if productList is empty or not.
			if(!productList.isEmpty()) {
				System.out.print("Product added!");
				return true;
			} else {
				return false;
			}
		}

		public boolean updateProduct(int row, ArrayList<String> product) {
			// check if productList is empty.
			if(productList.size() != 0) {
				for(int i = 0; i < productList.size(); i++) {
					for(int j = 0; j < productList.get(i).size(); j++) {
						// check if row matches with i to set new value.
						if(i == row) {
							System.out.println("Product updated!");
							productList.set(i, product);
							return true;
						}
					}
				}
			}
			return false;
		}

		public boolean deleteProduct(int row) {
			// check if productList is empty.
			if(productList.size() != 0) {
				// find which row to update.
				for (int i = 0; i < productList.size(); i++) {
					// if i is equals to row then remove that product from productList.
					if (i == row) {
						System.out.print("Product deleted.");
						productList.remove(i);
						return true;
					}
				}
			}
			return false;
		}

		public boolean getproductById(int id) {
			// check if 2d productList is empty.
			if(productList.size() != 0) {
				// find which id to match.
				for (int i = 0; i < productList.size(); i++) {
					// if i is equals to row then remove that product from productList.
					if (i == id) {
						System.out.println("Product name: " + productList.get(i).get(1));
						System.out.println("Product quantity: " + productList.get(i).get(2));
						System.out.println("Product price: " + productList.get(i).get(3));
						return true;
					}
				}
			}
			return false;
		}

		public boolean showallProducts() {
			// check if 2d productList is empty.
			if(productList.size() != 0) {
				// print all products..
				for(int i = 0; i < productList.size(); i++) {
					for(int j = 0; j < productList.get(i).size(); j++) {
						System.out.println("Product name: " + productList.get(i).get(1));
						System.out.println("Product quantity: " + productList.get(i).get(2));
						System.out.println("Product price: " + productList.get(i).get(3));
						// check if end of loop.
						if(i == productList.size() - 1){
							return true;
						}
					}
				}
			}
			return false;
		}
}
